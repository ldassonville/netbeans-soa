package net.openesb.netbeans.module.server.support.standalone.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.platform.Specification;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneProperties {

    /**
     * Java platform property which is used as a java platform ID
     */
    public static final String PLAT_PROP_ANT_NAME = "platform.ant.name"; //NOI18N

    private static final String PROP_URL = InstanceProperties.URL_ATTR;
    public static final String PROPERTY_ROOT_DIR = "root-dir";      //NOI18N
    public static final String PROPERTY_INSTANCE_NAME = "instance";      //NOI18N
    public static final String PROPERTY_ADMIN_PORT = "admin-port";      //NOI18N

    private static final String PROP_JAVA_PLATFORM = "java_platform";   // NOI18N
    private static final String PROP_DISPLAY_NAME = InstanceProperties.DISPLAY_NAME_ATTR;

    // default values
    public static final String DEF_VALUE_INSTANCE_NAME = "server";
    public static final int DEF_VALUE_ADMIN_PORT = 8699;
    public static final String DEF_VALUE_DISPLAY_NAME
            = NbBundle.getMessage(StandaloneProperties.class, "LBL_DefaultDisplayName");

    private final InstanceProperties ip;
    private final StandaloneDeploymentManager dm;
    private File homeDir;

    /**
     * Creates a new instance of StandaloneProperties
     */
    public StandaloneProperties(StandaloneDeploymentManager dm) {
        this.dm = dm;
        this.ip = dm.getInstanceProperties();

        String uri = ip.getProperty(PROP_URL); // NOI18N

        final String home = "home=";    // NOI18N
        int homeOffset = uri.indexOf(home) + home.length();
        String instanceHome = uri.substring(homeOffset, uri.length());

        if (instanceHome == null) {
            throw new IllegalArgumentException("OpenESB Standalone Home must not be null."); // NOI18N
        }

        homeDir = new File(instanceHome);

        if (!homeDir.isAbsolute()) {
            InstalledFileLocator ifl = InstalledFileLocator.getDefault();
            homeDir = ifl.locate(instanceHome, null, false);
        }
        if (homeDir == null || !homeDir.exists()) {
            throw new IllegalArgumentException("OpenESB Standalone directory does not exist."); // NOI18N
        }
    }

    public String getInstanceName() {
        String val = ip.getProperty(PROPERTY_INSTANCE_NAME);
        return val != null && val.length() > 0 ? val
                : DEF_VALUE_INSTANCE_NAME;
    }

    public String getDisplayName() {
        String val = ip.getProperty(PROP_DISPLAY_NAME);
        return val != null && val.length() > 0 ? val
                : DEF_VALUE_DISPLAY_NAME;
    }

    public int getAdminPort() {
        String val = ip.getProperty(PROPERTY_ADMIN_PORT);

        if (val != null) {
            try {
                return Integer.parseInt(val);
            } catch (NumberFormatException nfe) {
                Logger.getLogger(StandaloneProperties.class.getName()).log(Level.INFO, null, nfe);
            }
        }

        return DEF_VALUE_ADMIN_PORT;
    }

    public File getRootDir() {
        return homeDir;
    }

    public JavaPlatform getJavaPlatform() {
        String currentJvm = ip.getProperty(PROP_JAVA_PLATFORM);
        JavaPlatformManager jpm = JavaPlatformManager.getDefault();
        JavaPlatform[] installedPlatforms = jpm.getPlatforms(null, new Specification("J2SE", null)); // NOI18N
        for (int i = 0; i < installedPlatforms.length; i++) {
            String platformName = (String) installedPlatforms[i].getProperties().get(PLAT_PROP_ANT_NAME);
            if (platformName != null && platformName.equals(currentJvm)) {
                return installedPlatforms[i];
            }
        }
        // return default platform if none was set
        return jpm.getDefaultPlatform();
    }

    public List<URL> getClasses() {
        // OpenESB libs
        List<URL> retValue = new ArrayList<URL>();
        try {
            retValue.add(new File(getRootDir(), "javaee.jar").toURI().toURL());
        } catch (MalformedURLException ex) {
            Exceptions.printStackTrace(ex);
        }

        return retValue;
    }
    
    public List<URL> getJavadocs() {
        // OpenESB libs
        List<URL> retValue = new ArrayList<URL>();
        try {
            retValue.add(new File(getRootDir(), "javaee.jar").toURI().toURL());
        } catch (MalformedURLException ex) {
            Exceptions.printStackTrace(ex);
        }

        return retValue;
    }
    
    public List<URL> getSources() {
        // OpenESB libs
        List<URL> retValue = new ArrayList<URL>();
        try {
            retValue.add(new File(getRootDir(), "javaee.jar").toURI().toURL());
        } catch (MalformedURLException ex) {
            Exceptions.printStackTrace(ex);
        }

        return retValue;
    }
}
