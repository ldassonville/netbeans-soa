package org.netbeans.modules.sun.manager.jbi.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import org.openide.util.Exceptions;

/**
 *
 * @author David BRASSELY
 */
public class JBossServerInstance extends ServerInstance {

    @Override
    public List<File> getClasses() {
        List<File> classes = new ArrayList<File>();
        String serverLocation = this.getUrlLocation();

        // TODO: find a way to retrieve jboss5-jbi-install path from 
        // JBoss conf file
        String appserv_rt_jar = serverLocation + "/jboss5-jbi-install/lib/appserv-ext.jar"; // NOI18N

        File appserv_rt_jarFile = new File(appserv_rt_jar);
        if (appserv_rt_jarFile.exists()) {
            classes.add(appserv_rt_jarFile);
        } else {
            throw new RuntimeException("JbiClassLoader: Cannot find " + appserv_rt_jar + ".");
        }

        String jbi_rt_jar = serverLocation + "/jboss5-jbi-install/lib/jbi_rt.jar"; // NOI18N // Jboss

        File jbi_rt_jarFile = new File(jbi_rt_jar);
        if (jbi_rt_jarFile.exists()) {
            classes.add(jbi_rt_jarFile);
        } else {
            throw new RuntimeException("JbiClassLoader: Cannot find "
                    + jbi_rt_jar + ".");
        }
        
        return classes;
    }

    @Override
    public MBeanServerConnection getConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isJBIEnabled(MBeanServerConnection mBeanServerConnection) {
        try {
            return mBeanServerConnection.isRegistered(new ObjectName(
                    "openesb:service=Open-ESB"));
        } catch (Exception ex) {
            return false;
        }
    }
}
