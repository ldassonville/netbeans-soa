package net.openesb.netbeans.module.server.support.standalone;

import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.server.ServerInstance;
import org.netbeans.spi.server.ServerInstanceFactory;
import org.openide.nodes.Node;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class StandaloneInstanceImpl implements StandaloneInstance {

    private static final Logger logger = Logger.getLogger("StandaloneInstanceImpl");
    
    private String name;
    private final String location;
    private final String userName;
    private final String password;
    
    private transient InstanceContent ic;
    private transient Lookup lookup;
    private ServerInstance commonInstance;
    private StandaloneInstanceProvider provider;

    private ServerState state;
    private ChangeSupport changeSupport = new ChangeSupport(this);

    /*
    private StartServerTask startServer = new StartServerTask();
    private StopServerTask stopServer = new StopServerTask();
    private RestartTask restartServer = new RestartTask();
    */
    protected static final RequestProcessor RP = new RequestProcessor("OpenESB-Standalone",5); // NOI18N
    
    public StandaloneInstanceImpl(String name, String path, String username, String password) {
        this.name = name;
        this.location = path;
        this.userName = username;
        this.password = password;

        this.provider = StandaloneInstanceProvider.getProvider();
        init();
    }
    
    private void init() {
        commonInstance = ServerInstanceFactory.createServerInstance(this);
        ic = new InstanceContent();
        ic.add(this);
        lookup = new AbstractLookup(ic);
    }
    
    @Override
    public ServerState getState() {
        return state;
    }

    public void setState(ServerState state) {
        this.state = state;
        changeSupport.fireChange();
    }

    @Override
    public ServerInstance getCommonInstance() {
        return commonInstance;
    }

    @Override
    public boolean isRemovable() {
        return true;
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    @Override
    public void resultChanged(LookupEvent le) {
    }

    public void addChangeListener(ChangeListener listener) {
        changeSupport.addChangeListener(listener);
    }


    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public String getServerDisplayName() {
        return this.getDisplayName();
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void remove() {
        ic.remove(this);
        provider.remove(this);
    }

    @Override
    public void updateModuleSupport() {
    }

    @Override
    public void start() {
    //    RP.post(startServer);
    }

    @Override
    public void stop() {
    //    RP.post(stopServer);
    }

    @Override
    public void restart() {
    //    RP.post(restartServer);
    }

    @Override
    public Node getFullNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node getBasicNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JComponent getCustomizer() {
        return null;
    }   
}
