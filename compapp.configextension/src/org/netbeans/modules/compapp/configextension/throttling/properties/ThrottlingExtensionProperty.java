package org.netbeans.modules.compapp.configextension.throttling.properties;

import java.awt.Component;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.xml.namespace.QName;

import org.netbeans.modules.compapp.casaeditor.model.casa.CasaComponent;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaExtensibilityElement;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaWrapperModel;
import org.netbeans.modules.compapp.casaeditor.nodes.CasaNode;
import org.netbeans.modules.compapp.casaeditor.properties.spi.ExtensionProperty;
import org.netbeans.modules.compapp.configextension.throttling.model.ThrottlingAlgorithm;
import org.netbeans.modules.compapp.configextension.throttling.model.ThrottlingAlgorithm.Algorithm;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;


/**
 * @author ads
 *
 */
public class ThrottlingExtensionProperty extends ExtensionProperty<ThrottlingAlgorithm> {
    
    private static final QName ALGORITHM = new QName("algorithm");      // NOI18N
    
    private static final Logger LOG = Logger.getLogger( 
            ThrottlingExtensionProperty.class.getCanonicalName()); 
    
    public ThrottlingExtensionProperty( CasaNode node,
            CasaComponent extensionPointComponent,
            CasaExtensibilityElement firstEE, CasaExtensibilityElement lastEE,
            String propertyType, String propertyName, String displayName, 
            String description )
    {
        super(node, extensionPointComponent, firstEE, lastEE, propertyType, 
                ThrottlingAlgorithm.class,propertyName, displayName, description);
        myEditor = new ThrottlingEditor( );
        myEditor.setValue( getValue());
    }
    
    @Override
    public PropertyEditor getPropertyEditor() {
        return myEditor;
    }
    
    /* (non-Javadoc)
     * @see org.netbeans.modules.compapp.casaeditor.properties.spi.ExtensionProperty#getValue()
     */
    @Override
    public ThrottlingAlgorithm getValue() {
        CasaComponent component = getComponent();
        if ( component.getParent() == null ){
            return null;
        }
        
        String algorithm = component.getAnyAttribute(ALGORITHM);
        Algorithm alg = Algorithm.forString(algorithm);
        if ( alg == null ){
            alg = Algorithm.DISABLED;
        }
        String valueName = alg.getValueName();
        int value = 0;
        if ( valueName != null ){
            String attrValue = component.getAnyAttribute(new QName(valueName));
            try {
                value= Integer.parseInt(attrValue);
            }
            catch( NumberFormatException e ){
                Logger.getLogger( ThrottlingExtensionProperty.class.getCanonicalName()).
                    log( Level.FINE , null , e);
            }
        }
        return new ThrottlingAlgorithm( alg, value );
    }
    
    /* (non-Javadoc)
     * @see org.netbeans.modules.compapp.casaeditor.properties.spi.BaseCasaProperty#restoreDefaultValue()
     */
    @Override
    public void restoreDefaultValue() throws IllegalAccessException,
            InvocationTargetException
    {
        getPropertyEditor().setValue( null );
    }
    
    /* (non-Javadoc)
     * @see org.netbeans.modules.compapp.casaeditor.properties.spi.ExtensionProperty#setValue(java.lang.Object)
     */
    @Override
    public void setValue( ThrottlingAlgorithm value )
            throws IllegalAccessException, IllegalArgumentException,
            InvocationTargetException
    {
        CasaComponent component = getComponent();

        CasaWrapperModel model = getModel();
        
        if ( value == null || value.getAlgorithm() == null){
            if ( component.getParent() != null) {
                getModel().removeExtensibilityElement(extensionPointComponent,
                        (CasaExtensibilityElement)component);
            }
            return;
        }
        
        model.startTransaction();
        try {
            Algorithm algorithm = value.getAlgorithm();
            component.setAnyAttribute(ALGORITHM, algorithm.toString());
            
            for( Algorithm alg : Algorithm.values()){
                String valueName = alg.getValueName();
                if ( valueName != null ){
                    component.setAnyAttribute(new QName(valueName), null);
                }
            }
            if ( algorithm.getValueName() != null ){
                component.setAnyAttribute( new QName( algorithm.getValueName()), 
                        Integer.valueOf(value.getValue()).toString());
            }
        } 
        finally {
            if (model.isIntransaction()) {
                model.endTransaction();
            }
        }
        if (value!= null && component.getParent() == null) {
            // The extensibility element does not exist in the CASA model yet.
            getModel().addExtensibilityElement(extensionPointComponent, 
                    (CasaExtensibilityElement)component);
        }
    }
    
    private ThrottlingEditor myEditor;
    
    class ThrottlingEditor extends PropertyEditorSupport implements ExPropertyEditor {
        
        @Override
        public String getAsText() {
            ThrottlingAlgorithm value = (ThrottlingAlgorithm)getValue();
            if ( value == null || value.getAlgorithm() == null ){
                return Algorithm.DISABLED.toString();
            }
            Algorithm algorithm = value.getAlgorithm();
            String valueName = algorithm.getValueName();
            if ( valueName != null ){
                int intValue = value.getValue();
                StringBuilder builder = new StringBuilder( algorithm.toString() );
                builder.append(' ');
                builder.append( valueName );
                builder.append('=');
                builder.append( intValue );
                return builder.toString();
            }
            else {
                return algorithm.toString();
            }
        }

        /* (non-Javadoc)
         * @see org.openide.explorer.propertysheet.ExPropertyEditor#attachEnv(org.openide.explorer.propertysheet.PropertyEnv)
         */
        @Override
        public void attachEnv( PropertyEnv env ) {
            // Disable direct inline text editing.
            env.getFeatureDescriptor().setValue("canEditAsText", false); // NOI18N
            myEnv = env;
        }
        
        /* (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#supportsCustomEditor()
         */
        @Override
        public boolean supportsCustomEditor() {
            return true;
        }
        
        /* (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#getCustomEditor()
         */
        @Override
        public Component getCustomEditor() {
            return new ThrottlingCustomEditor( this , myEnv );
        }
        
        private PropertyEnv myEnv;
    }

}
