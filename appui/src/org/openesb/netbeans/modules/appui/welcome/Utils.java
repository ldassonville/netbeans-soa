package org.openesb.netbeans.modules.appui.welcome;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.openide.ErrorManager;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class Utils {
    
    private final static Logger LOGGER = Logger.getLogger(Utils.class.getName());
    
    /** Creates a new instance of Utils */
    private Utils() {
    }

    public static Graphics2D prepareGraphics(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Map rhints = (Map)(Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")); //NOI18N
        if( rhints == null && Boolean.getBoolean("swing.aatext") ) { //NOI18N
             g2.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
        } else if( rhints != null ) {
            g2.addRenderingHints( rhints );
        }
        return g2;
    }

    public static void showURL(String href) {
        boolean opened = false;
        
        if (DesktopUtils.isBrowseAvailable()) {
            try {
                URL url = new URL(href);
                DesktopUtils.browse(url.toURI());
                opened = true;
            } catch (Exception e) {
                LOGGER.throwing(Utils.class.getName(), "showURL", e);
            }
        }
        
        if (!opened)
            JOptionPane.showMessageDialog(null, "<html><b>Unable to launch web browser.</b><br><br>" + 
                    "Please open the following link manually:<br><code>" + href +
                    "</code></html>", "Unable To Launch Web Browser", JOptionPane.ERROR_MESSAGE);
    }

    static int getDefaultFontSize() {
        Integer customFontSize = (Integer)UIManager.get("customFontSize"); // NOI18N
        if (customFontSize != null) {
            return customFontSize.intValue();
        } else {
            Font systemDefaultFont = UIManager.getFont("TextField.font"); // NOI18N
            return (systemDefaultFont != null)
                ? systemDefaultFont.getSize()
                : 12;
        }
    }

    public static Color getColor( String resId ) {
        ResourceBundle bundle = NbBundle.getBundle("org.openesb.netbeans.modules.appui.welcome.resources.Bundle"); // NOI18N
        try {
            Integer rgb = Integer.decode(bundle.getString(resId));
            return new Color(rgb.intValue());
        } catch( NumberFormatException nfE ) {
            ErrorManager.getDefault().notify( ErrorManager.INFORMATIONAL, nfE );
            return Color.BLACK;
        }
    }
}
