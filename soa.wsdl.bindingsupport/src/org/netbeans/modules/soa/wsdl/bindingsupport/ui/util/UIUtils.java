package org.netbeans.modules.soa.wsdl.bindingsupport.ui.util;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

import org.openide.util.NbBundle;


/**
 * @author ads
 *
 */
public final class UIUtils {

    private UIUtils(){
    }
    
    public static void showDirectoryFileChooser(JTextComponent component, 
            Runnable runnable)
    {
        showDirectoryFileChooser(component , null , runnable );
    }
    
    public static void showDirectoryFileChooser(JTextComponent component, 
            JComponent parent, Runnable runnable)
    {
        JFileChooser chooser = new JFileChooser();
        initDirectoryFileChooser(chooser);
        showFileChooser(chooser, component, parent, runnable);
    }
    
    public static void showDirectoryFileChooser(JTextComponent component){
        showDirectoryFileChooser(component, null );
    }
    
    public static void showFileChooser(final JFileChooser chooser , 
            final JTextComponent component,final JComponent parent , final Runnable runnable) 
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                int retVal = chooser.showDialog(parent, NbBundle.getMessage( 
                        UIUtils.class, "LBL_Select")); //NOI18N
                if (retVal == javax.swing.JFileChooser.APPROVE_OPTION) {
                    component.setText(chooser.
                            getSelectedFile().getAbsolutePath());
                    if ( runnable != null ){
                        runnable.run();
                    }
                }
            }
        });
    }
    
    private static void initDirectoryFileChooser(JFileChooser chooser) {
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
    }
    
}
