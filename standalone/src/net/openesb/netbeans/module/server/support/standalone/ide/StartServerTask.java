package net.openesb.netbeans.module.server.support.standalone.ide;

import net.openesb.netbeans.module.server.support.standalone.StandaloneInstance;
import net.openesb.netbeans.module.server.support.standalone.StandaloneProcess;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StartServerTask implements Runnable {

    private final StandaloneInstance instance;
    private StandaloneProcess standalone = new StandaloneProcess();
    
    public StartServerTask(StandaloneInstance instance) {
        this.instance = instance;
    }
    
    @Override
        public void run() {
            StandaloneProcess process = standalone;
            process.setStandaloneHome(instance.getLocation());

            try {
                process.start();
/*
                LogViewer log = new LogViewer(process);
                log.print();
*/
                    instance.setState(StandaloneInstance.ServerState.STARTED);
            } catch (Exception e) {
                instance.setState(StandaloneInstance.ServerState.STOPPED);
            }
        }
}
