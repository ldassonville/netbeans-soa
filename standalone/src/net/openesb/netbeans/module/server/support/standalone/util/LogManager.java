package net.openesb.netbeans.module.server.support.standalone.util;

import java.io.InputStreamReader;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class LogManager {
    
    private ServerLog serverLog;
    private final StandaloneDeploymentManager manager;
    private final Object serverLogLock = new Object();
    
    /** Creates a new instance of LogManager */
    public LogManager(StandaloneDeploymentManager sdm) {
        manager = sdm;
    }
    
    /**
     * Open the server log (output).
     */
    public void openServerLog() {
        final Process process = manager.getStandaloneProcess();
        assert process != null;
        synchronized(serverLogLock) {
            if (serverLog != null) {
                serverLog.takeFocus();
                return;
            }
            serverLog = new ServerLog(
                manager,
                manager.getStandaloneProperties().getDisplayName(),
                new InputStreamReader(process.getInputStream()),
                new InputStreamReader(process.getErrorStream()),
                true,
                false);
            serverLog.start();
        }
        //PENDING: currently we copy only Tomcat std & err output. We should
        //         also support copying to Tomcat std input.
        new Thread() {
            public void run() {
                try {
                    process.waitFor();
                    Thread.sleep(2000);  // time for server log
                } catch (InterruptedException e) {
                } finally {
                    closeServerLog();
                }
            }
        }.start();
    }
    
    /**
     * Stop the server log thread, if started.
     */
    public void closeServerLog() {
        synchronized(serverLogLock) {
            if (serverLog != null) {
                serverLog.stop();
                serverLog = null;
            }
        }
    }

    /**
     * Can be the server log (output) displayed?
     *
     * @return <code>true</code> if the server log can be displayed, <code>false</code>
     *         otherwise.
     */
    public boolean hasServerLog() {
        return manager.getStandaloneProcess() != null;
    }
}
