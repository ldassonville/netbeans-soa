package net.openesb.netbeans.module.server.support.standalone;

import org.netbeans.api.server.ServerInstance;
import org.netbeans.spi.server.ServerInstanceImplementation;
import org.openide.util.Lookup;
import org.openide.util.LookupListener;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface StandaloneInstance extends ServerInstanceImplementation, Lookup.Provider, LookupListener{
    
    public enum ServerState {
        STARTING, STARTED, STOPPED;
    }

    public static final String NAME = "name";
    public static final String LOCATION = "location";
    public static final String USER_NAME = "user";
    public static final String PASSWORD = "password";

    public String getUserName();
    public String getPassword();
    
    public String getLocation();
    public ServerInstance getCommonInstance();
    public void updateModuleSupport();

    public ServerState getState();
    public void setState(ServerState state);

    public void start();
    public void stop();
    public void restart();
}
