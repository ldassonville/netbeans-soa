package net.openesb.netbeans.module.server.support.standalone.nodes.actions;

import net.openesb.netbeans.module.server.support.standalone.nodes.StandaloneInstanceNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 * Open OpenESB standalone output in the output window.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ViewServerLogAction extends NodeAction {

    @Override
    protected boolean enable(Node[] nodes) {
        for (int i = 0; i < nodes.length; i++) {
            StandaloneInstanceNode cookie = (StandaloneInstanceNode)nodes[i].getLookup().lookup(StandaloneInstanceNode.class);
            if (cookie == null || !cookie.hasServerLog()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
    
    @Override
    public String getName() {
        return NbBundle.getMessage(ViewServerLogAction.class, "LBL_OpenServerOutput"); // NOI18N
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    @Override
    protected void performAction(Node[] nodes) {
        for (int i = 0; i < nodes.length; i++) {
            StandaloneInstanceNode cookie = nodes[i].getLookup().lookup(StandaloneInstanceNode.class);
            if (cookie != null) {
                cookie.openServerLog();
            }
        }
    }    
}
