package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.LinkedList;
import javax.swing.Action;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.nodes.actions.ViewServerLogAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneInstanceNode extends AbstractNode implements Node.Cookie {
    
    private StandaloneDeploymentManager sdm;
    
    /**
     * Creates a new instance of StandaloneInstanceNode.
     * 
     * @param lookup will contain DeploymentFactory, DeploymentManager, Management objects. 
     */
    public StandaloneInstanceNode(Children children, Lookup lookup) {
        super(children);
        sdm = lookup.lookup(StandaloneDeploymentManager.class);
        setIconBaseWithExtension("net/openesb/netbeans/module/server/support/standalone/nodes/openesb.png"); // NOI18N
        getCookieSet().add(this);
    }
    
    @Override
    public String getShortDescription() {
        return NbBundle.getMessage(
                    StandaloneInstanceNode.class, 
                    "LBL_StandaloneInstanceNode");
    }
    
    @Override
    public javax.swing.Action[] getActions(boolean context) {
        java.util.List<Action> actions = new LinkedList<Action>();
        actions.add(SystemAction.get(ViewServerLogAction.class));
        return (SystemAction[])actions.toArray(new SystemAction[actions.size()]);
    }
    
    /**
     * Open the server log (output).
     */
    public void openServerLog() {
        sdm.logManager().openServerLog();
    }
    
    /**
     * Can be the server log (output) displayed?
     *
     * @return <code>true</code> if the server log can be displayed, <code>false</code>
     *         otherwise.
     */
    public boolean hasServerLog() {
        return sdm.isRunning(true);
    }
}
