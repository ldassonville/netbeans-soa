package net.openesb.netbeans.module.server.support.standalone.ide;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StopServerClass implements Runnable {

    @Override
    public void run() {
        /*
        try {
            cli.shutdown();
            setState(ServerState.STOPPED);
        } catch (IOException e) {
        }
        System.out.println("------------- STOP");
        */
    }
}
