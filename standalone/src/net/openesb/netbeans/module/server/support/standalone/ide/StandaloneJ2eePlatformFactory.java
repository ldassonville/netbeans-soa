package net.openesb.netbeans.module.server.support.standalone.ide;

import java.awt.Image;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;
import javax.enterprise.deploy.spi.DeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.modules.j2ee.deployment.common.api.J2eeLibraryTypeProvider;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformFactory;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformImpl;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformImpl2;
import org.netbeans.spi.project.libraries.LibraryImplementation;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class StandaloneJ2eePlatformFactory extends J2eePlatformFactory {

    private static final WeakHashMap<InstanceProperties,J2eePlatformImplImpl> instanceCache = new WeakHashMap<InstanceProperties,J2eePlatformImplImpl>();
    
    private StandaloneProperties sp;
    
    public synchronized J2eePlatformImpl getJ2eePlatformImpl(DeploymentManager dm) {
        assert StandaloneDeploymentManager.class.isAssignableFrom(dm.getClass()) : this + " cannot create platform for unknown deployment manager:" + dm;
        
        StandaloneDeploymentManager manager  = (StandaloneDeploymentManager) dm;
        InstanceProperties ip = manager.getInstanceProperties();
        
        if (ip == null) {
            throw new RuntimeException("Cannot create J2eePlatformImpl instance for " + manager.getUri()); // NOI18N
        }
        J2eePlatformImplImpl platform = instanceCache.get(ip);
        if (platform == null) {
            platform = new J2eePlatformImplImpl(manager.getStandaloneProperties());
            instanceCache.put(ip, platform);
        }
        return platform;
    }
    
    public static class J2eePlatformImplImpl extends J2eePlatformImpl2 {
        
        private final StandaloneProperties properties;

        public J2eePlatformImplImpl(StandaloneProperties properties) {
            this.properties = properties;
        }
        
        private LibraryImplementation[] libraries;
        
        @Override
        public File getServerHome() {
            return null;
        }

        @Override
        public File getDomainHome() {
            return null;
        }

        @Override
        public File getMiddlewareHome() {
            return null;
        }

        @Override
        public LibraryImplementation[] getLibraries() {
            if (libraries == null) {
                initLibraries();
            }
            return libraries.clone();
        }
        
        // private helper methods -------------------------------------------------

        private void initLibraries() {
            // create library
            LibraryImplementation lib = new J2eeLibraryTypeProvider().createLibrary();
            lib.setName("OpenESB");
            lib.setContent(J2eeLibraryTypeProvider.VOLUME_TYPE_CLASSPATH, properties.getClasses());
            lib.setContent(J2eeLibraryTypeProvider.VOLUME_TYPE_JAVADOC, properties.getJavadocs());
            lib.setContent(J2eeLibraryTypeProvider.VOLUME_TYPE_SRC, properties.getSources());
            libraries = new LibraryImplementation[] {lib};
        }

        @Override
        public String getDisplayName() {
            return NbBundle.getMessage(StandaloneJ2eePlatformFactory.class, "MSG_StandaloneServerPlatform");
        }

        @Override
        public Image getIcon() {
            return null;
        }

        @Override
        public File[] getPlatformRoots() {
            return new File[] {getServerHome(), getDomainHome()};
        }

        @Override
        public File[] getToolClasspathEntries(String string) {
            return new File[0];
        }

        @Override
        public boolean isToolSupported(String string) {
            return false;
        }

        @Override
        public Set<String> getSupportedJavaPlatformVersions() {
            Set<String> versions = new HashSet<String>();
            versions.add("1.5"); // NOI18N
            versions.add("1.6"); // NOI18N
            versions.add("1.7"); // NOI18N
            return versions;
        }

        @Override
        public JavaPlatform getJavaPlatform() {
            return JavaPlatformManager.getDefault().getDefaultPlatform();
        }
    }
}
