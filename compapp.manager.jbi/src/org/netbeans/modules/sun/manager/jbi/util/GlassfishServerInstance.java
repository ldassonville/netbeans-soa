package org.netbeans.modules.sun.manager.jbi.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.management.MBeanServerConnection;
import org.netbeans.modules.sun.manager.jbi.management.JBIFrameworkService;

/**
 *
 * @author David BRASSELY
 */
public class GlassfishServerInstance extends ServerInstance {

    @Override
    public List<File> getClasses() {
        List<File> classes = new ArrayList<File>();
        
        // Get the server location. This is not the domain or instance location. #90749
        String serverLocation = this.getUrlLocation(); 
        
        String appserv_rt_jar = serverLocation + "/lib/appserv-rt.jar"; // NOI18N
        
        File appserv_rt_jarFile = new File(appserv_rt_jar);
        if (appserv_rt_jarFile.exists()) {
            classes.add(appserv_rt_jarFile);
        } else {
            throw new RuntimeException("JbiClassLoader: Cannot find " + appserv_rt_jar + ".");
        }
        
        String jbi_rt_jar_90 = serverLocation + "/addons/jbi/lib/jbi_rt.jar"; // NOI18N // Glassfish 9.0
        String jbi_rt_jar_91 = serverLocation + "/jbi/lib/jbi_rt.jar"; // NOI18N // Glassfish 9.1
        
        File jbi_rt_jarFile = new File(jbi_rt_jar_90);
        if (jbi_rt_jarFile.exists()) {
            classes.add(jbi_rt_jarFile);
        } else {
            jbi_rt_jarFile = new File(jbi_rt_jar_91);
            if (jbi_rt_jarFile.exists()) {
                classes.add(jbi_rt_jarFile);
            } else {
                throw new RuntimeException("JbiClassLoader: Cannot locate " +
                        jbi_rt_jar_90 + " or " + jbi_rt_jar_91 + ".");
            }
        }
        
        return classes;
    }

    @Override
    public MBeanServerConnection getConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isJBIEnabled(MBeanServerConnection mBeanServerConnection) {
        JBIFrameworkService service =
                new JBIFrameworkService(mBeanServerConnection);
        return service.isJbiFrameworkEnabled();
    }
    
}
