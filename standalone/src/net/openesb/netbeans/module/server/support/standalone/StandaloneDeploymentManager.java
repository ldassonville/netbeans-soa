package net.openesb.netbeans.module.server.support.standalone;

import java.io.File;
import java.io.InputStream;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.model.DeployableObject;
import javax.enterprise.deploy.shared.DConfigBeanVersionType;
import javax.enterprise.deploy.shared.ModuleType;
import javax.enterprise.deploy.spi.DeploymentConfiguration;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.enterprise.deploy.spi.Target;
import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.exceptions.DConfigBeanVersionUnsupportedException;
import javax.enterprise.deploy.spi.exceptions.InvalidModuleException;
import javax.enterprise.deploy.spi.exceptions.TargetException;
import javax.enterprise.deploy.spi.status.ProgressObject;
import net.openesb.netbeans.module.server.support.standalone.util.LogManager;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import net.openesb.netbeans.module.server.support.standalone.util.Utils;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneDeploymentManager implements DeploymentManager {
    
    private static final Logger LOGGER = Logger.getLogger(StandaloneDeploymentManager.class.getName());
    
    /** uri of this DeploymentManager. */
    private final String uri;
    
    private InstanceProperties ip;
    
    private StandaloneProperties tp;
    
    /** System process of the started OpenESB Standalone */
    private Process process;
    
    private LogManager logManager = new LogManager(this);
    
    /** Creates an instance of connected StandaloneDeploymentManager.
     * 
     * @param uri URI for DeploymentManager
     */
    public StandaloneDeploymentManager(String uri) 
    throws IllegalArgumentException {
        LOGGER.log(Level.FINE, "Creating connected StandaloneDeploymentManager uri={0}", uri); //NOI18N
        this.uri = uri;
        ip = InstanceProperties.getInstanceProperties(getUri());
        if (ip != null) {
            tp = new StandaloneProperties(this);
        }
    }
    
    public InstanceProperties getInstanceProperties() {
        return ip;
    }
    
    public StandaloneProperties getStandaloneProperties() {
        return tp;
    }
    
    public int getAdminPort() {
        return tp.getAdminPort();
    }
    
    @Override
    public Target[] getTargets() throws IllegalStateException {
    /*    if (!isConnected ()) {
            throw new IllegalStateException ("StandaloneDeploymentManager.getTargets called on disconnected instance");   // NOI18N
        }
    */    
        // PENDING 
        return new StandaloneTarget [] { 
            new StandaloneTarget (uri, "OpenESB Standalone at "+uri, uri)
        };
    }

    @Override
    public TargetModuleID[] getRunningModules(ModuleType mt, Target[] targets) throws TargetException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TargetModuleID[] getNonRunningModules(ModuleType mt, Target[] targets) throws TargetException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TargetModuleID[] getAvailableModules(ModuleType mt, Target[] targets) throws TargetException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DeploymentConfiguration createConfiguration(DeployableObject d) throws InvalidModuleException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject distribute(Target[] targets, File file, File file1) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject distribute(Target[] targets, InputStream in, InputStream in1) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject distribute(Target[] targets, ModuleType mt, InputStream in, InputStream in1) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject start(TargetModuleID[] tmids) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject stop(TargetModuleID[] tmids) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject undeploy(TargetModuleID[] tmids) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isRedeploySupported() {
        return false;
    }

    @Override
    public ProgressObject redeploy(TargetModuleID[] tmids, File file, File file1) throws UnsupportedOperationException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProgressObject redeploy(TargetModuleID[] tmids, InputStream in, InputStream in1) throws UnsupportedOperationException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void release() {
    }

    @Override
    public Locale getDefaultLocale() {
        return Locale.getDefault();
    }

    @Override
    public Locale getCurrentLocale() {
        return Locale.getDefault();
    }

    @Override
    public void setLocale (Locale locale) throws UnsupportedOperationException {
    }

    @Override
    public Locale[] getSupportedLocales () {
        return Locale.getAvailableLocales ();
    }
    
    @Override
    public boolean isLocaleSupported (Locale locale) {
        if (locale == null) {
            return false;
        }
        
        Locale [] supLocales = getSupportedLocales ();
        for (int i =0; i<supLocales.length; i++) {
            if (locale.equals (supLocales[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public DConfigBeanVersionType getDConfigBeanVersion() {
        // PENDING 
        return null;
    }

    @Override
    public void setDConfigBeanVersion (DConfigBeanVersionType version) 
    throws DConfigBeanVersionUnsupportedException {
        if (!DConfigBeanVersionType.V1_3_1.equals (version)) {
            throw new DConfigBeanVersionUnsupportedException ("unsupported version");
        }
    }
    
    @Override
    public boolean isDConfigBeanVersionSupported (DConfigBeanVersionType version) {
        return DConfigBeanVersionType.V1_3_1.equals (version);
    }
    
    /**
     * Returns identifier of StandaloneDeploymentManager.
     * Be aware that this is not a real URI!
     * 
     * @return URI including home and base specification
     */
    public String getUri () {
        return this.uri;
    }
    
    /**
     * Set the <code>Process</code> of the started OpenESB Standalone.
     *
     * @param <code>Process</code> of the started OpenESB Standalone.
     */
    public synchronized void setStandaloneProcess(Process p) {
        process = p;
    }

    /**
     * Return <code>Process</code> of the started OpenESB Standalone.
     *
     * @return <code>Process</code> of the started OpenESB Standalone, <code>null</code> if
     *         OpenESB Standalone wasn't started by IDE.
     */
    public synchronized Process getStandaloneProcess() {
        return process;
    }
    
    public LogManager logManager() {
        return logManager;
    }
    
    /**
     * Returns true if the server is running.
     * 
     * @param checkResponse should be checked whether is the server responding - is really up?
     * @return <code>true</code> if the server is running.
     */
    public boolean isRunning(boolean checkResponse) {
        Process proc = getStandaloneProcess();
        if (proc != null) {
            try {
                // process is stopped
                proc.exitValue();
                return false;
            } catch (IllegalThreadStateException e) {
                // process is running
                    return true;
            }
        }
        
        if (checkResponse) {
            return Utils.ping(getAdminPort()); // is OpenESB responding?
        } else {
            return false; // cannot resolve the state
        }
    }
    
}
