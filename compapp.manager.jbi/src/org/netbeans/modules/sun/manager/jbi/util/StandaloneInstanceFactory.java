/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package org.netbeans.modules.sun.manager.jbi.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneInstanceFactory extends AbstractServerInstanceFactory {

    @Override
    public boolean canHandle(String instanceDisplayName) {
        return instanceDisplayName.contains("OpenESB Standalone");
    }

    @Override
    public ServerInstance create(Node instanceNode, boolean bln) {
        NodeList childNodes = instanceNode.getChildNodes();

        ServerInstance instance = new StandaloneServerInstance();
        instance.setHostName("localhost");
        
        for (int j = 0; j < childNodes.getLength(); j++) {
            Node childNode = childNodes.item(j);
            String childNodeName = childNode.getNodeName();

            if ((childNode.getNodeType() == Node.ELEMENT_NODE)
                    && (childNodeName.equalsIgnoreCase("attr"))) { // NOI18N
                Element attrElement = (Element) childNode;

                String key = attrElement.getAttribute("name");  // NOI18N
                String value = attrElement.getAttribute("stringvalue"); // NOI18N

                if (key.equalsIgnoreCase(DISPLAY_NAME)) {
                    instance.setDisplayName(value);
                } else if (key.equalsIgnoreCase("instance")) {
                    instance.setDomain(value);
                    instance.setTarget(value);
                } else if (key.equalsIgnoreCase(URL)) {
                    instance.setUrl(value);
                } else if (key.equalsIgnoreCase(USER_NAME)) {
                    instance.setUserName(value);
                } else if (key.equalsIgnoreCase(PASSWORD)) {
                    instance.setPassword(value);
                } else if (key.equalsIgnoreCase("root-dir")) {
                    instance.setUrlLocation(value);
                } else if (key.equalsIgnoreCase("admin-port")) {
                    instance.setAdminPort(value);
                }
            }
        }
        
        return instance;
    }
    
}
