package net.openesb.netbeans.module.server.support.standalone.ide;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.shared.ActionType;
import javax.enterprise.deploy.shared.CommandType;
import javax.enterprise.deploy.shared.StateType;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.enterprise.deploy.spi.Target;
import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.exceptions.OperationUnsupportedException;
import javax.enterprise.deploy.spi.status.ClientConfiguration;
import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.progress.ProgressEventSupport;
import net.openesb.netbeans.module.server.support.standalone.progress.Status;
import net.openesb.netbeans.module.server.support.standalone.util.LogManager;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import net.openesb.netbeans.module.server.support.standalone.util.Utils;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.modules.j2ee.deployment.plugins.api.ServerDebugInfo;
import org.netbeans.modules.j2ee.deployment.plugins.spi.StartServer;
import org.netbeans.modules.j2ee.deployment.profiler.api.ProfilerSupport;
import org.openide.execution.NbProcessDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

/**
 *
 * @author David BRASSELY
 */
public class StandaloneStartServer extends StartServer implements ProgressObject {

    public static final String OPENESB_BAT = "openesb.bat";    // NOI18N
    public static final String OPENESB_SH = "openesb.sh";     // NOI18N
    /**
     * Startup command tag.
     */
    public static final String TAG_EXEC_CMD = "openesb"; // NOI18N
    public static final String TAG_EXEC_STARTUP = "exec_startup"; // NOI18N
    public static final String TAG_EXEC_SHUTDOWN = "exec_shutdown"; // NOI18N
    /**
     * Normal mode
     */
    private static final int MODE_RUN = 0;
    /**
     * Debug mode
     */
    private static final int MODE_DEBUG = 1;
    /**
     * Profile mode
     */
    private static final int MODE_PROFILE = 2;
    /**
     * For how long should we keep trying to get response from the server.
     */
    private static final long TIMEOUT_DELAY = 180000;
    private static final Logger LOGGER = Logger.getLogger(StandaloneStartServer.class.getName());
    private static final int AVERAGE_SERVER_INSTANCES = 5;
    private static final RequestProcessor SERVER_CONTROL_RP = new RequestProcessor("OpenESB Standalone Control",
            AVERAGE_SERVER_INSTANCES);
    private final StandaloneDeploymentManager dm;
    private ProgressEventSupport pes;
    private static Map isDebugModeUri = Collections.synchronizedMap((Map) new HashMap(2, 1));

    public StandaloneStartServer(DeploymentManager dm) {
        if (!(dm instanceof StandaloneDeploymentManager)) {
            throw new IllegalArgumentException("Not an instance of StandaloneDeploymentManager"); // NOI18N
        }
        this.dm = (StandaloneDeploymentManager) dm;
        pes = new ProgressEventSupport(this);
    }

    @Override
    public boolean supportsStartDeploymentManager() {
        return true;
    }

    @Override
    public boolean supportsStartProfiling(Target target) {
        return false;
    }

    /**
     * Start Standalone OpenESB server if the StandaloneStartServer is not
     * connected.
     */
    @Override
    public ProgressObject startDeploymentManager() {
        LOGGER.log(Level.FINE, "StandaloneStartServer.startDeploymentManager called on {0}", dm);    // NOI18N
        pes.fireHandleProgressEvent(null, new Status(ActionType.EXECUTE, CommandType.START, "", StateType.RUNNING));
        SERVER_CONTROL_RP.post(new StartRunnable(MODE_RUN, CommandType.START),
                0, Thread.NORM_PRIORITY);
        isDebugModeUri.remove(dm.getUri());
        return this;
    }

    /**
     * Stops OpenESB standalone server. 
     * The DeploymentManager object will be disconnected.
     * All diagnostic should be communicated through ServerProgres with no 
     * exceptions thrown.
     * @return ServerProgress object used to monitor start server progress
     */
    @Override
    public ProgressObject stopDeploymentManager() {
        LOGGER.log(Level.FINE, "StandaloneStartServer.stopDeploymentManager called on {0}", dm);    // NOI18N
        pes.fireHandleProgressEvent(null, new Status(ActionType.EXECUTE, CommandType.STOP, "", StateType.RUNNING));
        SERVER_CONTROL_RP.post(new StartRunnable(MODE_RUN, CommandType.STOP),
                0, Thread.NORM_PRIORITY);
        isDebugModeUri.remove(dm.getUri());
        return this;
    }

    /**
     * Returns true if the admin server is also a target server (share the same
     * vm). Start/stopping/debug apply to both servers.
     *
     * @return true when admin is also target server
     */
    @Override
    public boolean isAlsoTargetServer(Target target) {
        return true;
    }

    /**
     * Returns true if the admin server should be started before configure.
     */
    @Override
    public boolean needsStartForConfigure() {
        return false;
    }

    /**
     * Returns true if the admin server should be started before asking for
     * target list.
     */
    @Override
    public boolean needsStartForTargetList() {
        return false;
    }

    /**
     * Returns true if the admin server should be started before administrative
     * configuration.
     */
    @Override
    public boolean needsStartForAdminConfig() {
        return false;
    }

    /**
     * Returns true if this admin server is running.
     */
    @Override
    public boolean isRunning() {
        return dm.isRunning(true);
    }

    /**
     * Returns true if this target is in debug mode.
     */
    @Override
    public boolean isDebuggable(Target target) {
        if (!isDebugModeUri.containsKey(dm.getUri())) {
            return false;
        }
        if (!isRunning()) {
            isDebugModeUri.remove(dm.getUri());
            return false;
        }
        return true;
    }

    @Override
    public ProgressObject startDebugging(Target target) {
        LOGGER.log(Level.FINE, "StandaloneStartServer.startDebugging called on " + dm);    // NOI18N
        pes.fireHandleProgressEvent(null, new Status(ActionType.EXECUTE, CommandType.STOP, "", StateType.RUNNING));
        SERVER_CONTROL_RP.post(new StartRunnable(MODE_DEBUG, CommandType.STOP),
                0, Thread.NORM_PRIORITY);
        return this;
    }

    @Override
    public ServerDebugInfo getDebugInfo(Target target) {
        return null;
    }

    @Override
    public DeploymentStatus getDeploymentStatus() {
        return pes.getDeploymentStatus();
    }

    @Override
    public TargetModuleID[] getResultTargetModuleIDs() {
        return new TargetModuleID[]{};
    }

    @Override
    public ClientConfiguration getClientConfiguration(TargetModuleID tmid) {
        return null;
    }

    @Override
    public boolean isCancelSupported() {
        return false;
    }

    @Override
    public void cancel() throws OperationUnsupportedException {
        throw new OperationUnsupportedException("");
    }

    @Override
    public boolean isStopSupported() {
        return false;
    }

    @Override
    public void stop() throws OperationUnsupportedException {
        throw new OperationUnsupportedException("");
    }

    @Override
    public void addProgressListener(ProgressListener pl) {
        pes.addProgressListener(pl);
    }

    @Override
    public void removeProgressListener(ProgressListener pl) {
        pes.removeProgressListener(pl);
    }

    private class StartRunnable implements Runnable {

        private int mode;
        private CommandType command = CommandType.START;

        public StartRunnable(int mode, CommandType command) {
            this.mode = mode;
            this.command = command;
        }

        public synchronized void run() {
            // PENDING check whether is runs or not
            StandaloneProperties sp = dm.getStandaloneProperties();
            File rootDir = sp.getRootDir();
            if (rootDir == null || !rootDir.exists()) {
                fireCmdExecProgressEvent(
                        command == CommandType.START ? "MSG_NoHomeDirStart" : "MSG_NoHomeDirStop",
                        StateType.FAILED);
                return;
            }

            // check whether the startup script - openesb.sh/bat exists
            File startupScript = getStartupScript();
            if (!startupScript.exists()) {
                final String MSG = NbBundle.getMessage(
                        StandaloneStartServer.class,
                        command == CommandType.START ? "MSG_StartFailedNoStartScript" : "MSG_StopFailedNoStartScript",
                        startupScript.getAbsolutePath());
                pes.fireHandleProgressEvent(
                        null,
                        new Status(ActionType.EXECUTE, command, MSG, StateType.FAILED));
                return;
            }

            int adminPort = dm.getAdminPort();

            if (command == CommandType.START) {
                // check whether the server ports are free
                if (!Utils.isPortFree(adminPort)) {
                    fireCmdExecProgressEvent("MSG_StartFailedServerPortInUse", String.valueOf(adminPort), StateType.FAILED);
                    return;
                }
            }

            JavaPlatform platform = getJavaPlatform();
            String jdkVersion = platform.getSpecification().getVersion().toString();

            try {
                NbProcessDescriptor pd = null;
                if (command == CommandType.START) {
                    pd = defaultExecDesc(TAG_EXEC_CMD, TAG_EXEC_STARTUP);
                } else {
                    pd = defaultExecDesc(TAG_EXEC_CMD, TAG_EXEC_SHUTDOWN);
                }

                fireCmdExecProgressEvent(command == CommandType.START ? "MSG_startProcess" : "MSG_stopProcess",
                        StateType.RUNNING);

                Process p = pd.exec(
                        new StandaloneFormat(startupScript, rootDir),
                        new String[]{
                            "JAVA_HOME=" + getJavaHome(platform), // NOI18N
                        //     "JRE_HOME=", // NOI18N ensure that JRE_HOME system property won't be used instead of JAVA_HOME
                        //     "JAVA_OPTS=" + javaOpts, // NOI18N
                        //     "CATALINA_HOME=" + homeDir.getAbsolutePath(), // NOI18N
                        //     "CATALINA_BASE=" + baseDir.getAbsolutePath(), // NOI18N
                        },
                        true,
                        new File(rootDir, "bin"));
                if (command == CommandType.START) {
                    dm.setStandaloneProcess(p);
                    openLogs();
                } else {
                    // #58554 workaround
                    //    SERVER_STREAMS_RP.post(new StreamConsumer(p.getInputStream()), 0, Thread.MIN_PRIORITY);
                    //    SERVER_STREAMS_RP.getDefault().post(new StreamConsumer(p.getErrorStream()), 0, Thread.MIN_PRIORITY);
                }
            } catch (java.io.IOException ioe) {
                LOGGER.log(Level.FINE, null, ioe);    // NOI18N
                fireCmdExecProgressEvent(command == CommandType.START ? "MSG_StartFailedIOE" : "MSG_StopFailedIOE",
                        startupScript.getAbsolutePath(), StateType.FAILED);
                return;
            }

            fireCmdExecProgressEvent("MSG_waiting", StateType.RUNNING);

            if (hasCommandSucceeded()) {
                if (command == CommandType.START) {
                    // reset the need restart flag
                //    dm.setNeedsRestart(false);
                    if (mode == MODE_DEBUG) {
                        isDebugModeUri.put(dm.getUri(), new Object());
                    }
                }
                fireCmdExecProgressEvent(command == CommandType.START ? "MSG_Started" : "MSG_Stopped",
                        StateType.COMPLETED);
            } else {
                fireCmdExecProgressEvent(command == CommandType.START ? "MSG_StartFailed" : "MSG_StopFailed",
                        StateType.FAILED);
            }
        }

        /**
         * Fires command progress event of action type
         * <code>ActionType.EXECUTE</code>.
         *
         * @param resName event status message from the bundle, specified by the
         * resource name.
         * @param stateType event state type.
         */
        private void fireCmdExecProgressEvent(String resName, StateType stateType) {
            String msg = NbBundle.getMessage(StandaloneStartServer.class, resName);
            pes.fireHandleProgressEvent(
                    null,
                    new Status(ActionType.EXECUTE, command, msg, stateType));
        }

        /**
         * Fires command progress event of action type
         * <code>ActionType.EXECUTE</code>.
         *
         * @param resName event status message from the bundle, specified by the
         * resource name.
         * @param arg1 the argument to use when formating the message
         * @param stateType event state type.
         */
        private void fireCmdExecProgressEvent(String resName, Object arg1, StateType stateType) {
            String msg = NbBundle.getMessage(StandaloneStartServer.class, resName, arg1);
            pes.fireHandleProgressEvent(
                    null,
                    new Status(ActionType.EXECUTE, command, msg, stateType));
        }

        /**
         * Return the standalone startup script file.
         */
        private File getStartupScript() {
            StandaloneProperties sp = dm.getStandaloneProperties();
            String startupScript = Utilities.isWindows() ? OPENESB_BAT : OPENESB_SH;
            return new File(sp.getRootDir(), "/bin/" + startupScript); // NOI18N
        }

        /**
         * Try to get response from the server, whether the START/STOP command
         * has succeeded.
         *
         * @return <code>true</code> if START/STOP command completion was
         * verified, <code>false</code> if time-out ran out.
         */
        private boolean hasCommandSucceeded() {
            long timeout = System.currentTimeMillis() + TIMEOUT_DELAY;
            while (true) {
                boolean isRunning = isRunning();
                if (command == CommandType.START) {
                    if (isRunning) {
                        return true;
                    }
                    if (isStopped()) {
                        // OpenESB Standalone failed to start, process is finished
                        return false;
                    }
                    if (mode == MODE_PROFILE) {
                        int state = ProfilerSupport.getState();
                        if (state == ProfilerSupport.STATE_BLOCKING
                                || state == ProfilerSupport.STATE_RUNNING
                                || state == ProfilerSupport.STATE_PROFILING) {
                            return true;
                        } else if (state == ProfilerSupport.STATE_INACTIVE) {
                            return false;
                        }
                    }
                }
                if (command == CommandType.STOP) {
                    if (isStopped()) {
                        // give server a few secs to finish its shutdown, not responding
                        // does not necessarily mean its is still not running
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ie) {
                        }
                        return true;
                    }
                }
                // if time-out ran out, suppose command failed
                if (System.currentTimeMillis() > timeout) {
                    return false;
                }
                try {
                    Thread.sleep(1000); // take a nap before next retry
                } catch (InterruptedException ie) {
                }
            }
        }

        /**
         * Return true if the server is stopped. If the server was started from
         * within the IDE, determin the server state from the process exit code,
         * otherwise try to ping it.
         */
        private boolean isStopped() {
            Process proc = dm.getStandaloneProcess();
            if (proc != null) {
                try {
                    proc.exitValue();
                    // process is stopped
                    return true;
                } catch (IllegalThreadStateException e) {
                    // process is still running
                    return false;
                }
            } else {
                return !Utils.ping(dm.getAdminPort());
            }
        }
        
        /** Open OpenESB standalone log */
        private void openLogs() {
            LogManager logManager = dm.logManager();
            logManager.closeServerLog();
            logManager.openServerLog();
        }
    }

    // private helper methods -------------------------------------------------    
    private static NbProcessDescriptor defaultExecDesc(String command, String argCommand) {
        return new NbProcessDescriptor(
                "{" + command + "}", // NOI18N
                "{" + argCommand + "}", // NOI18N
                NbBundle.getMessage(StandaloneStartServer.class, "MSG_StandaloneExecutionCommand"));
    }

    private JavaPlatform getJavaPlatform() {
        JavaPlatform platform = dm.getStandaloneProperties().getJavaPlatform();
        if (platform.getInstallFolders().size() <= 0) {
            LOGGER.log(Level.INFO, "The Java Platform used by OpenESB is broken; using the default one");
            return JavaPlatform.getDefault();
        }
        
        return platform;
    }

    private String getJavaHome(JavaPlatform platform) {
        FileObject fo = (FileObject) platform.getInstallFolders().iterator().next();
        return FileUtil.toFile(fo).getAbsolutePath();
    }

    /**
     * Format that provides value usefull for OpenESB Standalone execution. Currently this
     * is only the name of startup wrapper.
     */
    private static class StandaloneFormat extends org.openide.util.MapFormat {

        private static final long serialVersionUID = 992972967554321415L;

        public StandaloneFormat(File startupScript, File homeDir) {
            super(new java.util.HashMap());
            java.util.Map map = getMap();
            String scriptPath = startupScript.getAbsolutePath();
            map.put(TAG_EXEC_CMD, scriptPath);
            map.put(TAG_EXEC_STARTUP, "start");         // NOI18N
            map.put(TAG_EXEC_SHUTDOWN, "stop");        // NOI18N
        }
    }
}
