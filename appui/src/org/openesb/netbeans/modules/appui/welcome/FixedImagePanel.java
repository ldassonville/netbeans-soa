package org.openesb.netbeans.modules.appui.welcome;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import javax.swing.JPanel;


/**
 *
 * @author David BRASSELY
 */
class FixedImagePanel extends JPanel {
    
    protected Image image;
    protected Dimension imageSize;


    public FixedImagePanel(Image img) {
        try {
            image = loadImage(img, new MediaTracker(this));
            imageSize = new Dimension(image.getWidth(null), image.getHeight(null));
            setOpaque(false);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to load image: " + e.getMessage()); // NOI18N
        }
    }

    
    public Dimension getPreferredSize() {
        return imageSize;
    }
    
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
    
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
    
    
    protected void paintComponent(Graphics graphics) {
        graphics.drawImage(image, 0, 0, this);
    }

    
    private static Image loadImage(Image image, MediaTracker mTracker) throws InterruptedException {
        mTracker.addImage(image, 0);
        mTracker.waitForID(0);
        mTracker.removeImage(image, 0);
        return image;
    }
}
