package net.openesb.netbeans.module.server.support.standalone.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.openide.util.Exceptions;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class Utils {

    /**
     * Creates a new instance of Utils
     */
    private Utils() {
        super();
    }

    /**
     * Return true if the specified port is free, false otherwise.
     */
    public static boolean isPortFree(int port) {
        try {
            ServerSocket soc = new ServerSocket(port);
            try {
                soc.close();
            } finally {
                return true;
            }
        } catch (IOException ioe) {
            return false;
        }
    }

    /**
     * Return true if an OpenESB Standalone server is running on the specifed
     * port
     */
    public static boolean ping(int port) {
        JMXConnector conn = null;

        try {
            conn = JMXConnectorFactory.connect(new JMXServiceURL(
                    "service:jmx:rmi:///jndi/rmi://localhost:" + port + "/jmxrmi"));
            return true;
        } catch (IOException ex) {
            return false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }
}
