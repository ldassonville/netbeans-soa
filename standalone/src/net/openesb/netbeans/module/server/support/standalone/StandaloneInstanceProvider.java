package net.openesb.netbeans.module.server.support.standalone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.server.ServerInstance;
import org.netbeans.spi.server.ServerInstanceProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneInstanceProvider implements ServerInstanceProvider, LookupListener {
    
    private final static String INSTANCE_PATH = "/OpenESB/Standalone/Instances";
    
    private final Map<String, StandaloneInstance> instances =  Collections.synchronizedMap(new HashMap<String, StandaloneInstance>());
    private static final Set<String> activeDisplayNames = Collections.synchronizedSet(new HashSet<String>());

    private final ChangeSupport support = new ChangeSupport(this);
    private static StandaloneInstanceProvider provider;

    public static StandaloneInstanceProvider getProvider() {
        Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, "Instance provider: init()");
        if (provider == null) {
            provider = new StandaloneInstanceProvider();
            provider.init();
        }
        return provider;
    }
    
    public StandaloneInstance createInstance(String name, String location, String username, String password, boolean isDomain) {
        //create new instance
        StandaloneInstance server = new StandaloneInstanceImpl(name, location, username, password);

        //put into map
        instances.put(name, server);
        activeDisplayNames.add(name);

        //persist server's config
        try {
            this.writeInstanceToFile(server, true);
        } catch (Exception e) {
            Logger.getLogger("StandaloneInstanceProvider").log(Level.SEVERE, null, e);
        }

        support.fireChange();
        return server;
    }

    public void remove(StandaloneInstance instance) {
        instances.remove(instance.getDisplayName());
        activeDisplayNames.remove(instance.getDisplayName());

        try {
            removeInstanceFromFile(instance);
        } catch (Exception e) {
            Logger.getLogger("StandaloneInstanceProvider").log(Level.SEVERE, null, e);
        }

        support.fireChange();
    }
    
    @Override
    public List<ServerInstance> getInstances() {
        List<ServerInstance> result = new ArrayList<ServerInstance>();

        //init();
        synchronized (instances) {
            for (StandaloneInstance instance : instances.values()) {
                result.add(instance.getCommonInstance());
            }
        }
        Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, "Instance count: {0}", result.size());
        return result;
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
        Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, "addChangeListener()");
        support.addChangeListener(cl);
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
        support.removeChangeListener(cl);
    }

    @Override
    public void resultChanged(LookupEvent le) {
        Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, "ResultChanged()");
    }
    
    private void init() {
        synchronized (instances) {
            try {
                loadServerInstances();
            } catch (RuntimeException ex) {
                Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, null, ex);
            }
        }
    }
    
    public void loadServerInstances() {
        Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, "InstanceProvider: Load server instances");
        FileObject dir = this.getRepositoryDir(INSTANCE_PATH, false);

        if (dir != null) {
            FileObject[] instanceFOs = dir.getChildren();
            if (instanceFOs != null && instanceFOs.length > 0) {
                for (int i = 0; i < instanceFOs.length; i++) {
                    StandaloneInstance instance = this.readInstanceFromFile(instanceFOs[i]);
                    if (instance != null) {
                        instances.put(instance.getDisplayName(), instance);
                        activeDisplayNames.add(instance.getDisplayName());
                    }
                }
            }
        }

        for (StandaloneInstance i : instances.values()) {
            i.updateModuleSupport();
        }
    }
    
    private StandaloneInstance readInstanceFromFile(FileObject instanceFO) {
        String name = (String) instanceFO.getAttribute(StandaloneInstance.NAME);
        String location = (String) instanceFO.getAttribute(StandaloneInstance.LOCATION);
        String username = (String) instanceFO.getAttribute(StandaloneInstance.USER_NAME);
        String password = (String) instanceFO.getAttribute(StandaloneInstance.PASSWORD);

        return new StandaloneInstanceImpl(name, location, username, password);
    }
    
    private void writeInstanceToFile(StandaloneInstance instance, boolean search) throws IOException {
        FileObject dir = this.getRepositoryDir(INSTANCE_PATH, true);
        String name = FileUtil.findFreeFileName(dir, "instance", null);
        FileObject instanceFO = dir.createData(name);

        instanceFO.setAttribute(StandaloneInstance.NAME, instance.getDisplayName());
        instanceFO.setAttribute(StandaloneInstance.LOCATION, instance.getLocation());
        instanceFO.setAttribute(StandaloneInstance.USER_NAME, instance.getUserName());
        instanceFO.setAttribute(StandaloneInstance.PASSWORD, instance.getPassword());
    }
    
    private void removeInstanceFromFile(StandaloneInstance instance) throws IOException {
        FileObject dir = this.getRepositoryDir(INSTANCE_PATH, false);

        if (dir == null) {
            return;
        }

        FileObject[] instanceFOs = dir.getChildren();
        for (FileObject fo: instanceFOs) {
            String name = (String) fo.getAttribute(StandaloneInstance.NAME);
            if (name != null && name.equals(instance.getDisplayName())) {
                fo.delete();
            }
        }
    }
    
    private FileObject getRepositoryDir(String path, boolean create) {
        FileObject dir = FileUtil.getConfigFile(path);
        if (dir == null && create) {
            try {
                dir = FileUtil.createFolder(FileUtil.getConfigRoot(), path);
            } catch (IOException ex) {
                Logger.getLogger("StandaloneInstanceProvider").log(Level.INFO, null, ex);
            }
        }
        return dir;
    }
}
