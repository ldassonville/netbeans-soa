package net.openesb.netbeans.module.server.support.standalone.wizard;

import java.awt.Component;
import java.awt.Label;
import java.io.IOException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class AddInstanceIterator implements WizardDescriptor.InstantiatingIterator {

    public final static String PROP_ERROR_MESSAGE = WizardDescriptor.PROP_ERROR_MESSAGE; // NOI18N    
    private final static String PROP_CONTENT_DATA = WizardDescriptor.PROP_CONTENT_DATA;  // NOI18N
    private final static String PROP_CONTENT_SELECTED_INDEX = WizardDescriptor.PROP_CONTENT_SELECTED_INDEX; // NOI18N
    private final static String PROP_DISPLAY_NAME = "ServInstWizard_displayName"; // NOI18N
    
    private final static String[] CONTENT_DATA = new String[] { 
        NbBundle.getMessage(AddInstanceIterator.class, "LBL_InstallationDetails") };
    
    private InstallPanel panel;
    private WizardDescriptor wizard;

    public void removeChangeListener(ChangeListener l) {
    }

    public void addChangeListener(ChangeListener l) {
    }

    public void uninitialize(WizardDescriptor wizard) {
    }

    public void initialize(WizardDescriptor wizard) {
        this.wizard = wizard;
    }

    public void previousPanel() {
        throw new NoSuchElementException();
    }

    public void nextPanel() {
        throw new NoSuchElementException();
    }

    public String name() {
        return null;
    }

    public Set instantiate() throws IOException {
        Set result = new HashSet();
        String displayName = getDisplayName();
        String url = panel.getVisual().getUrl();
        String username = "username"; // NOI18N
        String password = "password"; // NOI18N
        try {
            InstanceProperties ip = InstanceProperties.createInstanceProperties(
                    url, username, password, displayName);
            
            ip.setProperty(StandaloneProperties.PROPERTY_INSTANCE_NAME, StandaloneProperties.DEF_VALUE_INSTANCE_NAME);
            ip.setProperty(StandaloneProperties.PROPERTY_ADMIN_PORT, Integer.toString(StandaloneProperties.DEF_VALUE_ADMIN_PORT));
            ip.setProperty(StandaloneProperties.PROPERTY_ROOT_DIR, panel.getVisual().getHomeDir().getAbsolutePath());
            
            result.add(ip);
        } catch (Exception ex) {
            DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(ex.getMessage(),
                    NotifyDescriptor.ERROR_MESSAGE));
        }
        return result;
    }

    public boolean hasPrevious() {
        return false;
    }

    public boolean hasNext() {
        return false;
    }

    public WizardDescriptor.Panel current() {
        if (panel == null) {
            panel = new InstallPanel();
        }
        setContentData((JComponent)panel.getComponent());
        setContentSelectedIndex((JComponent)panel.getComponent());
        return panel;
    }
    
    private void setContentData(JComponent component) {
        if (component.getClientProperty(PROP_CONTENT_DATA) == null) {
            component.putClientProperty(PROP_CONTENT_DATA, CONTENT_DATA);
        }
    }

    private void setContentSelectedIndex(JComponent component) {
        if (component.getClientProperty(PROP_CONTENT_SELECTED_INDEX) == null) {
            component.putClientProperty(PROP_CONTENT_SELECTED_INDEX, Integer.valueOf(0));
        }
    }

    private String getDisplayName() {
        return (String) wizard.getProperty(PROP_DISPLAY_NAME);
    }
}
