package org.openesb.netbeans.modules.appui.welcome;

import java.awt.BorderLayout;
import java.awt.Image;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;

/**
 *
 * @author David BRASSELY
 */
class FooterPanel extends JPanel {
    
    private static final String BOTTOM_LEFT_RESOURCE =    "org/openesb/netbeans/modules/appui/welcome/resources/welcome-bottomleft.png";
    private static final String BOTTOM__RIGHT_RESOURCE =  "org/openesb/netbeans/modules/appui/welcome/resources/welcome-bottomright.png";
    private static final String BOTTOM__MIDDLE_RESOURCE = "org/openesb/netbeans/modules/appui/welcome/resources/welcome-bottommiddle.png";
    
    
    public FooterPanel() {
        initComponents();
    }
    
    
    private void initComponents() {
        Image bottomLeftImage = ImageUtilities.loadImage(BOTTOM_LEFT_RESOURCE, true);
        Image bottomRightImage = ImageUtilities.loadImage(BOTTOM__RIGHT_RESOURCE, true);
        Image bottomMiddleImage = ImageUtilities.loadImage(BOTTOM__MIDDLE_RESOURCE, true);
        
        setLayout(new BorderLayout());
        setOpaque(false);
        add(new FixedImagePanel(bottomLeftImage), BorderLayout.WEST);
        add(new FixedImagePanel(bottomRightImage), BorderLayout.EAST);
        add(new HorizontalImagePanel(bottomMiddleImage), BorderLayout.CENTER);
    }

}
