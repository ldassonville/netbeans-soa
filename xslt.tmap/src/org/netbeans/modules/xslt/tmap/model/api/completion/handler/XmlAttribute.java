/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.netbeans.modules.xslt.tmap.model.api.completion.handler;

/**
 *
 * @author lativ
 */
public class XmlAttribute extends XmlElement{
    private String mValue;
    private String mName;
    
    public XmlAttribute(String name) {
        mName = name;
    }

    public XmlAttribute(String name, String value) {
        mName = name;
        setValue(value);
    }
    
    public final void setValue(String value) {
        if (getState()) {
            throw new IllegalStateException("attribute value yet setted");
        }
        mValue = value;
        setState(true);
    }

    public String getValue() {
        return mValue;
    }
    
    public String getName() {
        return mName;
    }
}
