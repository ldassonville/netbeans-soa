package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.netbeans.modules.j2ee.deployment.plugins.spi.RegistryNodeFactory;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneRegistryNodeFactory implements RegistryNodeFactory {
    
    private static final Logger LOGGER = Logger.getLogger(StandaloneRegistryNodeFactory.class.getName());
    
    /** Creates a new instance of StandaloneRegistryNodeFactory */
    public StandaloneRegistryNodeFactory() {
    }

    /**
      * Return node representing the admin server.
      * Children of this node are filtered.
      * 
      * @param lookup will contain DeploymentFactory, DeploymentManager, Management objects. 
      * @return admin server node.
      */
    @Override
    public Node getManagerNode(Lookup lookup) {
        StandaloneInstanceNode tn = new StandaloneInstanceNode (new Children.Map(), lookup);
        return tn;
    }
    
    /**
      * Provide node representing JSR88 Target object.  
      * @param lookup will contain DeploymentFactory, DeploymentManager, Target, Management objects.
      * @return target server node
      */
    @Override
    public Node getTargetNode(Lookup lookup) {
        return new TargetNode(lookup, getExtensionNodes(lookup));
    }
    
    private Node[] getExtensionNodes(Lookup lookup) {
        List<Node> nodes = new ArrayList<Node>();
        
        MBeanServerConnection conn = getConnection(lookup);
        
        if (conn != null) {
            for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
                if (nep != null) {

                     Node node = nep.getExtensionNode(conn);
                     if (node != null){
                         nodes.add(node);
                     }
                 }
            }
        }
        
        return (Node[]) nodes.toArray(new Node[nodes.size()]);
    }
    
    private MBeanServerConnection getConnection(Lookup lookup) {
        StandaloneDeploymentManager dm = lookup.lookup(StandaloneDeploymentManager.class);

        try {
            try {
                JMXConnector conn = JMXConnectorFactory.connect(new JMXServiceURL(
                        "service:jmx:rmi:///jndi/rmi://localhost:" + dm.getAdminPort() + "/jmxrmi"));

                return conn.getMBeanServerConnection();
            } catch (IOException ex) {
            //    LOGGER.log(Level.INFO, ex.getMessage(), ex);
            }
        } catch (Exception ex) {
        //    LOGGER.log(Level.INFO, ex.getMessage(), ex);
        }
        
        return null;
    }
}
